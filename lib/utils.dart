/// Maps a percentage to a phrase.
Map<int, List<String>> phrases = <int, List<String>>{
  206: <String>['more than double', ''],
  195: <String>['about double', ''],
  180: <String>['nearly double', ''],
  161: <String>['more than 1.5 times', ''],
  145: <String>['about 1.5 times', ''],
  135: <String>['about 1.4 times', ''],
  128: <String>['about 1.3 times', ''],
  122: <String>['about 25 percent higher', 'than'],
  115: <String>['about 20 percent higher', 'than'],
  107: <String>['about 10 percent higher', 'than'],
  103: <String>['a little higher', 'than'],
  98: <String>['about the same as', ''],
  94: <String>['a little less', 'than'],
  86: <String>['about 90 percent', 'of'],
  78: <String>['about 80 percent', 'of'],
  72: <String>['about three-quarters', 'of'],
  64: <String>['about two-thirds', 'of'],
  56: <String>['about three-fifths', 'of'],
  45: <String>['about half', ''],
  37: <String>['about two-fifths', 'of'],
  30: <String>['about one-third', 'of'],
  23: <String>['about one-quarter', 'of'],
  17: <String>['about one-fifth', 'of'],
  13: <String>['less than a fifth', 'of'],
  8: <String>['about 10 percent', 'of'],
  0: <String>['less than 10 percent', 'of'],
};

/// Converts the provided ratio to a rounded percentage value
/// and returns the phrase mapped to the nearest lower percentage
/// in [phrases] map.
///
/// Eg: nearest to 5 is 0 and not 8 since 0 is lower
/// Eg: nearest to 8 is 8 and not 10 since 8 is lower
List<String> nearest(num ratio) {
  final int roundedRatio = (ratio * 100).round();

  final int nearest = phrases.keys.reduce((int x, int y) {
    if (roundedRatio >= x)
      return x;
    else
      return y;
  });

  return phrases[nearest];
}

/// Takes a string representation of a number and
/// formats the given number with commas and returns it.
/// Eg: 123456789 => 1,23,456,789
///
/// Note: Since Dart doesn't have lookbehind support in regex
/// a counter and a loop is used.
String formatNumber(String numberString) {
  final StringBuffer formattedString = new StringBuffer();

  int counter = 1;
  for (int i = numberString.length; i > 0; i--) {
    if (counter % 4 == 0) {
      formattedString.write(',${numberString[i-1]}');
      counter += 2;
    } else {
      formattedString.write(numberString[i - 1]);
      counter++;
    }
  }

  return formattedString.toString().split('').reversed.join();
}
