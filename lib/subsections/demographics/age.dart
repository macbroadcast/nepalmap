import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on "Age" of the population.
class Age extends StatelessWidget {
  /// Contains data of key "demographics" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [Age] widget.
  const Age(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          const SubSectionHeader('Age'),
          new StatList(data['median_age'], 'float'),
          new DistributionChart(data['age_category_distribution'],
              title: 'Population by age category', pie: true),
          new DistributionChart(data['age_group_distribution'],
              title: 'Population by 10-year age range'),
          const Divider(),
        ],
      ),
    );
  }
}
