import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on home ownership.
class HomeOwnership extends StatelessWidget {
  /// Contains value of key "households" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [HomeOwnership].
  const HomeOwnership(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Home Ownership'),
        new StatList(
          data['own_home'],
          'percentage',
        ),
        new DistributionChart(
          data['home_ownership_distribution'],
          title: 'Households by ownership',
        ),
        const Divider(),
      ],
    );
  }
}
