import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on education level of the population.
class EducationLevel extends StatelessWidget {
  /// Contains value of key "education" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [EducationLevel] widget.
  const EducationLevel(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Education Level Passed'),
        new StatList(data['primary_level_reached'], 'percentage'),
        new DistributionChart(
          data['education_level_passed_distribution'],
          title: 'Highest education level reached',
        ),
        const Divider(),
      ],
    );
  }
}
