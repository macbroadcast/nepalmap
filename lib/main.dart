import 'package:flutter/material.dart';
import 'package:NepalMap/containers/homepage.dart';

void main() => runApp(new MyApp());

/// It is the root widget in the widget treewhich returns a [MaterialApp]
/// and home widget as [HomePageContainer]
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'NepalMap',
      theme: new ThemeData(
        primaryColor: const Color(0xFFE00015),
      ),
      home: new HomePageContainer(),
    );
  }
}

/// It is a [Scaffold] with [HomePage] widget as the body
class HomePageContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new HomePage(),
    );
  }
}
