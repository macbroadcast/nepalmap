import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/subsections/business/registeration_decade.dart';
import 'package:NepalMap/subsections/business/registeration_era.dart';
import 'package:flutter/material.dart';

/// Returns a [ExpandableList] with business data.
Widget getBusiness(Map<dynamic, dynamic> data) {
  final List<Widget> business = <Widget>[];

  if (data['area_has_data'] != null && data['area_has_data'] == true) {
    business.addAll(<Widget>[
      new RegisterationDecade(data),
      new RegisterationEra(data),
    ]);

    return new ExpandableList(
      title: 'Business',
      children: business,
    );
  }

  return new Container();
}
