import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:flutter/material.dart';

/// Returns a [ExpandableList] with disasters data.
Widget getDisasters(Map<dynamic, dynamic> data) {
  final List<Widget> disasters = <Widget>[];

  if (data['area_has_data'] != null && data['area_has_data'] == true) {
    disasters.addAll(<Widget>[
      new StatList(
        data['flood_deaths'],
        'number',
        title: 'Disaster-related deaths',
      ),
    ]);

    return new ExpandableList(
      title: 'Disasters',
      children: disasters,
    );
  }

  return Container();
}
