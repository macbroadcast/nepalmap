import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/subsections/education/education_level.dart';
import 'package:NepalMap/subsections/education/education_level_by_sex.dart';
import 'package:NepalMap/subsections/education/field_of_study.dart';
import 'package:NepalMap/subsections/education/literacy.dart';
import 'package:NepalMap/subsections/education/school_attendace.dart';
import 'package:flutter/material.dart';

/// Returns a [ExpandableList] with education data.
Widget getEducation(Map<dynamic, dynamic> data) {
  final List<Widget> education = <Widget>[];

  if (data['area_has_data'] != null && data['area_has_data'] == true) {
    education.addAll(<Widget>[
      new StatList(
        data['aged_five_and_over'],
        'number',
        title: 'Education',
      ),
      new Literacy(data),
      new EducationLevel(data),
      new EducationLevelBySex(data),
    ]);

    if (data['is_vdc'] != null && data['is_vdc'] == false) {
      education.add(new FieldOfStudy(data));
    }
    education.add(new SchoolAttendace(data));

    return new ExpandableList(
      title: 'Education',
      children: education,
    );
  }

  return new Container();
}
