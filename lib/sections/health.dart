import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/subsections/health/assisted_childbirth.dart';
import 'package:flutter/material.dart';

/// Returns a [ExpandableList] with health data.
Widget getHealth(Map<dynamic, dynamic> data) {
  final List<Widget> health = <Widget>[];

  if (data['area_has_data'] != null && data['area_has_data'] == true) {
    health.addAll(<Widget>[
      new StatList(
        data['children_malnourished'],
        'percentage',
        title: 'Child Health',
      ),
      new StatList(
        data['percent_with_safe_water'],
        'percentage',
        title: 'Safe Drinking Water',
      ),
      new DistributionChart(
        data['safe_water_dist'],
        title: 'Access to Safe Drinking Water',
        pie: true,
      ),
      const Divider(),
      new StatList(
        data['births_at_health_facilities'],
        'percentage',
        title: 'Childbirth at a Health Facility',
      ),
      new StatList(
        data['births_with_sba'],
        'percentage',
        title: 'Childbirth with a Skilled Attendant',
      ),
      new StatList(
        data['births_with_health_worker'],
        'percentage',
        title: 'Childbirth with a non-SBA Health Worker',
      ),
      const Divider(),
      new AssistedChildbirth(data),
    ]);

    return new ExpandableList(
      title: 'Health',
      children: health,
    );
  }

  return new Container();
}
