import 'dart:async';
import 'dart:math';

import 'package:NepalMap/codeMappings.dart';
import 'package:NepalMap/components/customroute.dart';
import 'package:NepalMap/components/homepage_button.dart';
import 'package:NepalMap/components/search_ui.dart';
import 'package:NepalMap/containers/profile.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';

/// Home page for the app.
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  double headerHeight = 150.0, buttonHeight = 50.0;
  bool showTextField = false;
  bool animationOff = true;

  AnimationController slideAnimationController;
  Animation<double> slideAnimation;

  @override
  void initState() {
    super.initState();

    slideAnimationController = new AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this,
    );
  }

  Future<Null> _playAnimation() async {
    try {
      await slideAnimationController.forward().orCancel;
    } on TickerCanceled {
      // the animation got canceled, probably because we were disposed
    }
  }

  Future<Null> _reverseAnimation() async {
    try {
      await slideAnimationController.reverse().orCancel;
    } on TickerCanceled {
      // the animation got canceled, probably because we were disposed
    }
  }

  @override
  Widget build(BuildContext context) {
    slideAnimation = new Tween<double>(
      begin: headerHeight - (buttonHeight / 2.0),
      end: MediaQuery.of(context).padding.top,
    ).animate(slideAnimationController);

    return new Container(
      child: new Stack(
        children: <Widget>[
          new Container(
            color: Theme.of(context).primaryColor,
            height: headerHeight,
            child: const Center(
              child: const Text(
                'NepalMap',
                style: const TextStyle(
                  fontSize: 50.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          new GestureDetector(
            onTap: () {
              setState(() {
                animationOff = false;
              });

              _playAnimation().then((Future<Null> value) {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return new Container(
                      child: new SearchUI(),
                    );
                  },
                ).whenComplete(() {
                  setState(() {
                    animationOff = true;
                  });

                  _reverseAnimation();
                });
              });
            },
            child: new StaggeredRect(
              animation: slideAnimation,
              controller: slideAnimationController.view,
            ),
          ),
          new HomeButton(
            'Feeling lucky!',
            top: headerHeight + buttonHeight,
            margin: 10.0,
            enabled: animationOff,
            onPressed: () {
              final int length = codeMappings.length;
              final Random random = new Random();
              final int randomNumber = random.nextInt(length);
              final String randomKey =
                  codeMappings.keys.elementAt(randomNumber);

              Navigator.of(context).push(
                    new CustomRoute<Profile>(
                      builder: (_) => new Profile(
                            code: randomKey,
                            name: codeMappings[randomKey],
                          ),
                    ),
                  );
            },
          ),
          new HomeButton(
            'Browse Nepal',
            top: headerHeight + buttonHeight * 2.5,
            margin: 10.0,
            enabled: animationOff,
            onPressed: () => Navigator.of(context).push(
                  new CustomRoute<Profile>(
                    builder: (_) => const Profile(
                          code: 'country-NP',
                          name: 'Nepal',
                        ),
                  ),
                ),
          ),
        ],
      ),
    );
  }
}

/// Animates the widget's size using the povided controllers.
class StaggeredRect extends StatelessWidget {
  /// Base animation variable passed from [HomePage].
  final Animation<double> animation;

  /// Base animation controller passed from [HomePage].
  final AnimationController controller;

  /// Animation variable to animate the left and right margin of the widget.
  final Animation<double> leftRightMargin;

  /// Default constructor for [StaggeredRect].
  StaggeredRect({
    Key key,
    @required this.animation,
    @required this.controller,
  })  : leftRightMargin = new Tween<double>(
          begin: 10.0,
          end: 0.0,
        ).animate(controller),
        super(key: key);

  Widget _buildAnimation(BuildContext context, Widget child) {
    return new Card(
      elevation: 2.0,
      margin: new EdgeInsets.only(
        top: animation.value,
        left: leftRightMargin.value,
        right: leftRightMargin.value,
      ),
      child: new Container(
        height: 58.0 - leftRightMargin.value,
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: new Row(
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.all(10.0),
              child: const Icon(Icons.search),
            ),
            const Expanded(
              child: const Text(
                'Find a place in Nepal...',
                style: const TextStyle(fontSize: 18.0),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new AnimatedBuilder(
      builder: _buildAnimation,
      animation: controller,
    );
  }
}
