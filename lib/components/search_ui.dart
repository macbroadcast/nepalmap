import 'dart:async';

import 'package:flutter/material.dart';
import 'package:NepalMap/codeMappings.dart';
import 'package:NepalMap/containers/profile.dart';
import 'package:NepalMap/components/customroute.dart';

/// A search bar with animated search results.
class SearchUI extends StatefulWidget {
  @override
  _SearchUIState createState() => new _SearchUIState();
}

class _SearchUIState extends State<SearchUI> with TickerProviderStateMixin {
  TextEditingController _searchTextController;
  StreamController<List<Widget>> _streamController;
  Stream<List<Widget>> _onAdd;

  _SearchUIState() {
    _searchTextController = new TextEditingController();

    _streamController = new StreamController<List<Widget>>();
    _onAdd = _streamController.stream;
  }

  void _showResult(String searchString) {
    final List<Widget> results = <Widget>[];

    int counter = 0;
    codeMappings.forEach((dynamic key, dynamic value) {
      if (counter >= 5) return;

      if (searchString.length > 2 &&
          value.toLowerCase().contains(searchString)) {
        counter++;
        results.add(
          new GestureDetector(
            onTap: () {
              Navigator.pop(context);

              Navigator.of(context).push(
                    new CustomRoute<Profile>(
                      builder: (_) => new Profile(code: key, name: value),
                    ),
                  );
            },
            child: new Container(
              decoration: new BoxDecoration(
                border: new Border.all(color: Colors.white),
              ),
              padding:
                  const EdgeInsets.symmetric(horizontal: 5.0, vertical: 12.0),
              child: new Row(
                children: <Widget>[
                  new Text(
                    value,
                    style: const TextStyle(fontSize: 20.0),
                  ),
                  new Expanded(
                    child: new Align(
                      alignment: Alignment.centerRight,
                      child: new Text(
                        key.split('-')[0],
                        style: const TextStyle(
                          fontSize: 18.0,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }
    });

    _streamController.add(results);
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Scaffold(
        backgroundColor: const Color(0x00000000),
        body: new Column(
          children: <Widget>[
            new Container(
              color: Colors.white,
              padding: const EdgeInsets.all(5.0),
              child: new Row(
                children: <Widget>[
                  new IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  new Expanded(
                    child: new TextField(
                      style:
                          const TextStyle(fontSize: 18.0, color: Colors.black),
                      controller: _searchTextController,
                      onChanged: (String value) {
                        _showResult(value.toLowerCase());
                      },
                      autofocus: true,
                      decoration: const InputDecoration.collapsed(
                        hintStyle: const TextStyle(fontSize: 18.0),
                        hintText: 'Find a place in Nepal...',
                      ),
                    ),
                  ),
                ],
              ),
            ),
            new StreamBuilder<List<Widget>>(
              stream: _onAdd,
              builder:
                  (BuildContext context, AsyncSnapshot<List<Widget>> snapshot) {
                if (!snapshot.hasData) {
                  return new Container();
                }

                return new AnimatedSize(
                  vsync: this,
                  duration: const Duration(milliseconds: 200),
                  child: new AnimatedContainer(
                    duration: const Duration(milliseconds: 100),
                    color: Colors.white,
                    child: new Column(
                      children: snapshot.data,
                    ),
                  ),
                );
              },
            ),
            new Expanded(
              child: new GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
